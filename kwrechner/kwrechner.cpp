#include <iostream>
#include <string>
using namespace std;

class datum {
private:
	int tag;
	int monat;
	int jahr;
	bool schaltjahr;
public:
	int getTag();
	int getMonat();
	int getJahr();
	int wochentag();
	//void setJahr(int);
	//void setMonat(int);
	//void setJahr(int);
	datum(int, int, int);
	datum();
	~datum();
	bool schaltjahrBestimmen(int);
	int kwBerechnen();
	void ausgeben();
};

int datum::getTag()
{
	return tag;
}

int datum::getMonat()
{
	return monat;
}

int datum::getJahr()
{
	return jahr;
}

bool datum::schaltjahrBestimmen(int jahr) {
	bool sjahr = 0;
	if (!((jahr % 4) || !(jahr % 100)) || !((jahr % 4) || (jahr % 400)))
		sjahr = 1;
	return sjahr;
}

datum::datum() {
	tag = 1;
	monat = 1;
	jahr = 2020;
	schaltjahr = schaltjahrBestimmen(jahr);
}

datum::datum(int t, int m, int j) {

	tag = t;
	monat = m;
	jahr = j;
	schaltjahr = schaltjahrBestimmen(j);

}

datum::~datum() {

}

void datum::ausgeben() {
	cout << tag << " tag " << monat << " Monat " << jahr << " Jahr " << schaltjahr << endl;
}

int datum::wochentag()
{
	int year = jahr;
	int m = monat;
	if (m < 3)
		year--;

	string yearst = to_string(year);
	string cst = yearst.substr(0, 2);
	string yst = yearst.substr(2, 4);
	int y = stoi(yst);
	int c = stoi(cst);
	int d = tag;
	

	if (m == 1 || m == 2)
		m += 10;
	else
		m -= 2;

	int w = (d + (int)(2.6 * m - 0.2) + y + (int)(y / 4) + (int)(c / 4) - 2 * c) % 7;
	if (w < 0)
		w += 7;

	return w;
}

int datum::kwBerechnen()
{
	int monate[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int tage = 0;

	if (monat == 1)
		tage = tag;
	else
	{
		for (int i = 0; i < monat - 1; i++)
			tage += monate[i];
		tage += tag;
	}

	if (schaltjahrBestimmen(jahr) == 1 && monat > 2)
		tage += 1;


	datum ersteJan(1, 1, jahr);
	int wochentagErste = ersteJan.wochentag();
	int offset = 0;
	int kw = 0;

	if (wochentagErste < 4) {
		offset = wochentagErste - 2;
		kw += 1;
	}
		
	else {
		offset = (8 - wochentagErste) % 7;
		offset = 7 - (offset+1);
	}
	

	kw += ((tage + offset) / 7);

	if (kw == 0)
	{
		datum datum2(31, 12, jahr - 1);
		if (datum2.kwBerechnen() == 52)
			kw = 52;
		else
			kw = 53;
	}

	else if (kw == 53)
	{
		datum datum1(1, 1, jahr + 1);
		if (datum1.wochentag() < 4)
			kw = 1;
	}

	return kw;
}


int main()
{
	int tag, monat, jahr;
	cout << "Datum: ";
	string str;
	getline(cin, str);

	tag = stoi(str.substr(0, str.find('.')));
	monat = stoi(str.substr(str.find('.')+1, str.find('.')+1));
	jahr = stoi(str.substr(str.find('.', 3) + 1));

	//cout << "tag: " << tag << "monat: " << monat << "jahr " << jahr;
	datum datum2(tag, monat, jahr);
	int w = datum2.kwBerechnen();
	cout << "kw: " << w;
	

}