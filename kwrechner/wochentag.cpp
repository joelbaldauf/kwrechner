#include <iostream>
#include <string>
using namespace std;

class datum {
private:
	int tag;
	int monat;
	int jahr;
	bool schaltjahr;
public:
	int getTag();
	int getMonat();
	int getJahr();
	int wochentag();
	//void setJahr(int);
	//void setMonat(int);
	//void setJahr(int);
	datum(int, int, int);
	datum();
	~datum();
	bool schaltjahrBestimmen(int);
	int kwBerechnen();	
	void ausgeben();
};

int datum::getTag()
{
	return tag;
}

int datum::getMonat()
{
	return monat;
}

int datum::getJahr()
{
	return jahr;
}

bool datum::schaltjahrBestimmen(int jahr) {
	bool sjahr = 0;
	if (!((jahr % 4) || !(jahr % 100)) || !((jahr % 4) || (jahr % 400)))
		bool sjahr = 1;
	return sjahr;
}

datum::datum() {
	tag = 1;
	monat = 1;
	jahr = 2020;
	schaltjahr = schaltjahrBestimmen(jahr);
}

datum::datum(int t, int m, int j) {

	tag = t;
	monat = m;
	jahr = j;
	schaltjahr = schaltjahrBestimmen(j);

}

datum::~datum() {
	
}

void datum::ausgeben() {
	cout << tag << " tag " << monat << " Monat " << jahr << " Jahr " << schaltjahr << endl;
}

int datum::wochentag()
{
	string yearst = to_string(jahr);
	string cst = yearst.substr(0, 2);
	string yst = yearst.substr(2, 4);
	int y = stoi(yst);
	int c = stoi(cst);
	int d = tag;
	int m = monat;

	if (m < 3)
		y--;

	if (m == 1 || m == 2)
		m += 10;
	else
		m -= 2;
	
	int w = (d + (int)(2.6 * m - 0.2) + y + (int)(y / 4) + (int)(c / 4) - 2 * c) % 7;
	if (w < 0)
		w += 7;

	return w;
}

int datum::kwBerechnen()
{
	int monate[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	int tage = 0;

	if (monat == 1)
		tage = tag;
	else
	{
		for (int i = 0; i < monat - 1; i++)
			tage += monate[i];
		tage += tag;
	}

	if (schaltjahrBestimmen(jahr) == 1 && monat > 2)
		tage += 1;


	datum ersteJan(1, 1, jahr);
	int wochentagErste = ersteJan.wochentag();
	int offset = 0;
	int kw = 0;

	if (wochentagErste < 4)
		offset = wochentagErste - 2;
	else
		offset = (8 - wochentagErste) % 7;
	kw += 1;
	
	kw += ((tage + offset) / 7);

	if (kw == 0)
		kw = 53;
	
	if (kw == 53)
	{
		datum datum1(1, 1, jahr + 1);
		if (datum1.kwBerechnen() == 1 && datum1.wochentag()<4)
			kw = 1;
	}

	return kw;
}


int main()
{
	datum datum2 (30, 12, 2019);
	int w = datum2.kwBerechnen();
	cout << "kw: " << w;
}